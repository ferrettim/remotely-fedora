**Remotely-Fedora**

A quick script to get [Remotely](https://remotely.one) server up and running on Fedora 32.

To use it, simply download and run it via terminal.

`git clone git clone git@gitlab.com:ferrettim/remotely-fedora.git`\
`cd remotely-fedora`\
`sh remotely-fedora.sh`

After the script is done executing, point your browser to localhost:5000 or whatever domain you set up.
