#!/bin/bash
echo "Thanks for trying Remotely!"
echo

read -p "Enter path where the Remotely server files should be installed (typically /var/www/remotely): " appRoot
if [ -z "$appRoot" ]; then
    appRoot="/var/www/remotely"
fi

read -p "Enter server host (e.g. remotely.yourdomainname.com): " serverHost

read -p "Enter user the host will run as (typically www-data): " run_as

# Install .NET Core Runtime.
dnf -y install dotnet-sdk-3.1
dnf -y install apt-transport-https
dnf -y install aspnetcore-runtime-3.1


 # Install other prerequisites.
dnf -y install unzip
dnf -y install acl
dnf -y install ffmpeg
dnf -y install glibc-devel
dnf -y install libgdiplus


# Download and install Remotely files.
mkdir -p $appRoot
wget "https://github.com/Jay-Rad/Remotely/releases/latest/download/Remotely_Server_Linux-x64.zip"
unzip -o Remotely_Server_Linux-x64.zip -d $appRoot
rm Remotely_Server_Linux-x64.zip
setfacl -R -m u:$run_as:rwx $appRoot
chown -R u:$run_as $appRoot


# Install Nginx
dnf update
dnf -y install nginx

systemctl start nginx


# Configure Nginx
sed -i "s/server_name   ;/server_name   $serverHost *.$serverHost;/g" remotely
sed -i "s/root  ;/root  $appRoot;/g" remotely
cp remotely /etc/nginx/sites-available/remotely
ln -s /etc/nginx/sites-available/remotely /etc/nginx/sites-enabled/remotely

# Test config.
nginx -t

# Reload.
nginx -s reload




# Create service.

serviceConfig="[Unit]
Description=Remotely Server

[Service]
WorkingDirectory=$appRoot
ExecStart=/usr/bin/dotnet $appRoot/Remotely_Server.dll
Restart=always
# Restart service after 10 seconds if the dotnet service crashes:
RestartSec=10
SyslogIdentifier=remotely
User=www-data
Environment=ASPNETCORE_ENVIRONMENT=Production
Environment=DOTNET_PRINT_TELEMETRY_MESSAGE=false

[Install]
WantedBy=multi-user.target"

echo "$serviceConfig" > /etc/systemd/system/remotely.service


# Enable service.
systemctl enable remotely.service
# Start service.
systemctl restart remotely.service

dotnet $appRoot/Remotely_Server.dll


# Install Certbot and get SSL cert.
dnf -y install certbot python-certbot-nginx

certbot --nginx
